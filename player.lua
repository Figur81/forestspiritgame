player =  {
   x= 005,
   y= 014,
   vx= 0,
   vy= 0,
   colorkey= {val=0, min=-1, max=15},
   scale= {val=1, min=0, max=256},
   flip= {val=0, min=0, max=3},
   rotate= {val=0, min=0, max=3},
   w= {val=2, min=0, max=3},
   h= {val=2, min=0, max=3},
   frame= 256
 }


function solid(x,y)
  return solids[mget((x)//7.5, (y)//7.5)]
end

function player.update()
  if btn(2) then 
      player.vx=-1
  elseif btn(3) then 
      player.vx=1
  else 
    player.vx=0
  end
      
    if solid(player.x+player.vx, player.y+player.vy) 
    or solid(player.x+7+player.vx, player.y+player.vy) 
    or solid(player.x+player.vx, player.y+7+player.vy) 
    or solid(player.x+7+player.vx, player.y+7+player.vy) then
      player.vx = 0
    end
      
    if solid(player.x+player.vx, player.y+8+player.vy) 
    or solid(player.x+7+player.vx, player.y+8+player.vy) then
      player.vy = 0
    else
      player.vy = player.vy + 0.2
    end
      
    if player.vy == 0 and btn(4) then 
      player.vy =- 2.5
    end
  
    if player.vy < 0 and (solid(player.x+player.vx, player.y+player.vy) 
    or solid(player.x+7+player.vx, player.y+player.vy)) then
      player.vy = 0
    end  
  
    player.x=player.x+player.vx
    player.y=player.y+player.vy
end

return player	